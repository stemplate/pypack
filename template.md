# Stemplate

*Answer a few questions that the user might have when arriving on this page.*

> What does it mean?

The name "Stemplate" comes from the contraction of "Stem" and "Template".

> What is it for?

Stemplate is a useless Python package.

> Is it hard to use?

Just type one of the available commands in a terminal.

## Background

*Present the context and the problem addressed.*

## Features

*Present what the package can do by listing the main commands.*

* `head`: To read the first lines of a file.
* `date`: To display the date.

## Installation

You can find how to install the package in the [installation section](https://stemplate.gitlab.io/pypack/practice/installation/index.html) of the documentation.

## Usage

You can find how to use the package in the [examples section](https://stemplate.gitlab.io/pypack/practice/examples/index.html) of the documentation.

## Credits

* Author Name
* Contributor Name

## License

*The license text.*

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
