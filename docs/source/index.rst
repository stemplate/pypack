Stemplate package documentation
===============================

Welcome!
This documentation has been carefully prepared to help you use the Stemplate package.
The :doc:`first section <practice/index>` provides installation instructions and usage examples.
The :doc:`second section <package/stemplate>` documents the package code.
Hopefully you will find the information you are looking for!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   practice/index
   package/stemplate

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
