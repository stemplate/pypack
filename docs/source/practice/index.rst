stemplate in practice
=====================

This section provides installation instructions and usage examples.
If you are here for the first time, it is recommended to read the pages in order.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation/index
   examples/index
