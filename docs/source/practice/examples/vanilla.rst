Vanilla example
===============

Stemplate contains a default configuration of almost all parameters.
You will see later how to use the command-line interface or your own configuration file to replace the default values.
For now, and thanks to all the preconfigured parameters, you will be able to test the package features with a minimum of effort.

Display the date
~~~~~~~~~~~~~~~~

To display the date, run the following command:

.. code-block:: bash

   stemplate date

Read the first lines of a file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To display the first few lines of a file, run the following command:

.. code-block:: bash

   stemplate head <path-to-the-file>
