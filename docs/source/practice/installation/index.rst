Installation guide
==================

You will find here all the information and advice for the installation of the package.
Don't worry, it shouldn't take long!

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   venv
   regular
   editable
