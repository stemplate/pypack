Regular installation
====================

.. note::

   You can install the package in two different ways: with the possibility or not to modify the source files.
   If you don't want to modify the source files, you can follow the instructions on this page and skip the next page.
   If you want to install the package in editable mode, you can skip this page and go directly to the next page.

Stemplate is released on the `Python Package Index <https://pypi.org/project/stemplate>`_.
Thus, the easiest way to install the package is to use the `package installer for Python <https://pip.pypa.io/en/stable/>`_ with the following command:

.. code-block:: bash

   pip install stemplate

That's all!
The installation is complete and you can start using the package with the examples provided in the examples section.
