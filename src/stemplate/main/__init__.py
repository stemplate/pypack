# -*- coding: utf-8 -*-

"""Main features of Stemplate.

This package gathers the features designed to be called directly by the
user via a Python script or through the command-line interface.

"""
